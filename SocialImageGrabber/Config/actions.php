<?php
return [
    'social_image_grabber' => [
        'title' => ['ru' => 'Доступ к модулю', 'en' => 'Access module', 'es' => 'Módulo de acceso'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-key'
    ],
    'social_image_approve' => [
        'title' => ['ru' => 'Утвержденные изображения', 'en' => 'Approved image', 'es' => 'Aprobado imagen'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-image-o'
    ],
    'social_image_rejected' => [
        'title' => ['ru' => 'Отклоненные изображения', 'en' => 'Rejected image', 'es' => 'Imagen rechazada'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-folder-image'
    ],
    'social_image_request' => [
        'title' => ['ru' => 'Новые изображения', 'en' => 'New images', 'es' => 'Nuevas imágenes'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-search-replace zmdi-hc-fw'
    ]
];