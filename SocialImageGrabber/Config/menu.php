<?php
return [
    'package' => 'solutions_social_image_grabber',
    'title' => ['ru' => 'Сборщик изображений', 'en' => 'Image Grabber', 'es' => 'Colector imagen'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-vk',
    'menu_child' => [
        'social_image_approve' => [
            'title' => ['ru' => 'Утвержденные', 'en' => 'Approved image', 'es' => 'Aprobado imagen'],
            'route' => 'solutions.social_image_grabber.approved',
            'icon' => 'zmdi zmdi-collection-image-o',
        ],
        'social_image_rejected' => [
            'title' => ['ru' => 'Отклоненные', 'en' => 'Rejected image', 'es' => 'Imagen rechazada'],
            'route' => 'solutions.social_image_grabber.rejected',
            'icon' => 'zmdi zmdi-collection-folder-image',
        ],
        'social_image_request' => [
            'title' => ['ru' => 'Новые', 'en' => 'New images', 'es' => 'Nuevas imágenes'],
            'route' => 'solutions.social_image_grabber.request',
            'icon' => 'zmdi zmdi-search-replace zmdi-hc-fw',
        ]
    ]
];