<?php
return [
    'social_image_grabber' => [
        'title' => ['ru' => 'Сборщик изображений', 'en' => 'Image Grabber', 'es' => 'Colector imagen'],
        'options' => [
            ['group_title' => ['ru' => 'Социальные сети', 'en' => 'Social networks', 'es' => 'Redes sociales']],
            'vkontakte' => [
                'title' => [
                    'ru' => 'Вконтакте',
                    'en' => 'Vkontakte',
                    'es' => 'Vkontakte'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'checkbox',
                'value' => TRUE
            ],
            'twitter' => [
                'title' => [
                    'ru' => 'Твиттер',
                    'en' => 'Twitter',
                    'es' => 'Twitter'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'checkbox',
                'value' => TRUE
            ],
            ['group_title' => ['ru' => 'Настройки Twitter', 'en' => 'Twitter Settings', 'es' => 'Configuración de Twitter']],
            'twitter_token' => [
                'title' => ['ru' => 'OAuth Access Token', 'en' => 'OAuth Access Token', 'es' => 'OAuth Access Token'],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => ''
            ],
            'twitter_token_secret' => [
                'title' => ['ru' => 'OAuth Access Token Secret', 'en' => 'OAuth Access Token Secret', 'es' => 'OAuth Access Token Secret'],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => ''
            ],
            'twitter_consumer_key' => [
                'title' => ['ru' => 'Consumer key', 'en' => 'Consumer key', 'es' => 'Consumer key'],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => ''
            ],
            'twitter_consumer_secret' => [
                'title' => ['ru' => 'Consumer Secret', 'en' => 'Consumer Secret', 'es' => 'Consumer Secret'],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => ''
            ]
        ]
    ]
];