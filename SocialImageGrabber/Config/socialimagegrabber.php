<?php
return [
    'package_name' => 'solutions_social_image_grabber',
    'package_title' => ['ru' => 'Сборщик изображений из соц.сетей', 'en' => 'Social Image Grabber', 'es' => 'Redes sociales colector'],
    'package_icon' => 'zmdi zmdi-vk',
    'relations' => [],
    'package_description' => [
        'ru' => 'Позволяет собирать изображения из социальных сетей. Используются социальные сети: Вконтакте, Twitter',
        'en' => 'It allows you to collect images of the social networks. Use social networks: Vkontakte, Twitter',
        'es' => 'Se le permite recoger imágenes de las redes sociales. Utilizar las redes sociales: Vkontakte, Twitter'
    ],
    'version' => [
        'ver' => 1.1,
        'date' => '26.09.2016'
    ],
    'users_profiles' => [
        'vkontakte' => 'https://vk.com/id',
        'twitter' => 'https://twitter.com/',
    ]
];