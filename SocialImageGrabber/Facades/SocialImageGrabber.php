<?php
namespace STALKER_CMS\Solutions\SocialImageGrabber\Facades;

use Illuminate\Support\Facades\Facade;

class SocialImageGrabber extends Facade {

    protected static function getFacadeAccessor() {

        return 'ImageImportController';
    }
}