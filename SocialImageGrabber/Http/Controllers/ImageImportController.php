<?php
namespace STALKER_CMS\Solutions\SocialImageGrabber\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Solutions\SocialImageGrabber\Traits\CurlTrait;

class ImageImportController extends ModuleController {

    use CurlTrait;
    protected $model;
    private $ContentLength;

    public function __construct() {

        $this->ContentLength = 0;
    }

    public function vkontakte($search) {

        $start_time = Carbon::now()->subDays(1)->timestamp;
        $end_time = Carbon::now()->timestamp;
        $url = "https://api.vk.com/method/newsfeed.search?v=5.44&q=%23$search&extended=1&count=200&start_time=$start_time&end_time=$end_time&fields=id,first_name,last_name,photo_200";
        $result = $this->getCurl($url);
        $result = json_decode($result['curl_result']);
        $images = new Collection();
        if(count($result->response->items)):
            foreach($result->response->items as $index => $item):
                if($item->from_id > 0 && isset($item->attachments)):
                    $image_item = ['item_id' => $item->id, 'user_id' => $item->owner_id, 'image_links' => [], 'likes' => @$item->likes->count];
                    foreach($item->attachments as $attachment):
                        if($attachment->type == 'photo' && isset($attachment->photo->photo_604)):
                            $image_item['image_links'][$attachment->photo->id] = $attachment->photo->photo_604;
                        endif;
                    endforeach;
                    $images->add($image_item);
                endif;
            endforeach;
        endif;
        if(FALSE && $images->count()):
            foreach($images as $index => $image):
                $url = 'https://api.vk.com/method/users.get?user_id='.$image['user_id'].'&v=5.44&fields=id,first_name,last_name,photo_400_orig&lang='.\App::getLocale();
                $result = $this->getCurl($url);
                $result = json_decode($result['curl_result']);
                if(isset($result->response[0])):
                    $image['user'] = [
                        'id' => @$result->response[0]->id,
                        'name' => @$result->response[0]->first_name.' '.@$result->response[0]->last_name,
                        'image' => @$result->response[0]->photo_400_orig
                    ];
                    $images->put($index, $image);
                endif;
            endforeach;
        endif;
        return $images;
    }

    public function twitter($search) {

        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        $request = '?q=#'.$search.'&count=200&result_type=mixed';
        $config = [
            'oauth_access_token' => settings(['solutions_social_image_grabber', 'social_image_grabber', 'twitter_token']),
            'oauth_access_token_secret' => settings(['solutions_social_image_grabber', 'social_image_grabber', 'twitter_token_secret']),
            'consumer_key' => settings(['solutions_social_image_grabber', 'social_image_grabber', 'twitter_consumer_key']),
            'consumer_secret' => settings(['solutions_social_image_grabber', 'social_image_grabber', 'twitter_consumer_secret'])
        ];
        $twitter = new TwitterAPIExchange($config);
        $result = $twitter->setGetfield($request)->buildOauth($url, 'GET')->performRequest();
        $result = json_decode($result);
        $images = new Collection();
        if(count($result->statuses)):
            foreach($result->statuses as $index => $item):
                if(!empty($item->user) && !empty($item->entities->media)):
                    foreach($item->entities->media as $media):
                        if(isset($media->type) && $media->type == 'photo'):
                            $image_item = [
                                'item_id' => $item->id, 'user_id' => $item->user->id,
                                'user' => [
                                    'id' => $item->user->id,
                                    'name' => !empty($item->user->screen_name) ? $item->user->screen_name : $item->user->name,
                                    'image' => $item->user->profile_image_url
                                ],
                                'image_links' => [$media->id => $media->media_url],
                                'likes' => @$item->favorite_count
                            ];
                            $images->add($image_item);
                        endif;
                    endforeach;
                endif;
            endforeach;
        endif;
        return $images;
    }
}