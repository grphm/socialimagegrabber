<?php
namespace STALKER_CMS\Solutions\SocialImageGrabber\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Solutions\SocialImageGrabber\Models\SocialImageGrabber;
use STALKER_CMS\Vendor\Http\Controllers\Controller;

class ModuleController extends Controller {

    protected $model;

    public function __construct(SocialImageGrabber $model) {

        $this->model = $model;
        $this->middleware('auth');
    }

    public function approved() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_approve');
        $images = $this->model->whereApprove(TRUE)->orderBy('created_at', 'DESC');
        if(\Request::has('social')):
            $images = $images->where('social_title', \Request::input('social'));
        endif;
        $total_images = $images->count();
        $images = $images->paginate(30);
        return view('solutions_social_image_grabber_views::approved', compact('images', 'total_images'));
    }

    public function approveImage() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_approve');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['image_id' => 'required|numeric', 'approve' => 'required|numeric'])):
            $this->model->replace($request::input('image_id'), $request);
            if($request::has('redirect')):
                return \ResponseController::success(200)->redirect(route($request::get('redirect')))->json();
            else:
                return \ResponseController::success(200)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function approveSelectImages() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_approve');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['approve' => 'required|numeric'])):
            $this->model->whereIn('id', $request::input('images'))->update(['approve' => $request::input('approve')]);
            if($request::has('redirect')):
                return \ResponseController::success(200)->redirect(route($request::get('redirect')))->json();
            else:
                return \ResponseController::success(200)->redirect(route('solutions.social_image_grabber.request'))->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function rejected() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_rejected');
        $images = $this->model->whereApprove(FALSE)->orderBy('created_at', 'DESC');
        if(\Request::has('social')):
            $images = $images->where('social_title', \Request::input('social'));
        endif;
        $total_images = $images->count();
        $images = $images->paginate(30);
        return view('solutions_social_image_grabber_views::rejected', compact('images', 'total_images'));
    }

    public function rejectImage() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_rejected');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['image_id' => 'required|numeric', 'approve' => 'required|numeric'])):
            $this->model->replace($request::input('image_id'), $request);
            if($request::has('redirect')):
                return \ResponseController::success(200)->redirect(route($request::get('redirect')))->json();
            else:
                return \ResponseController::success(200)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function rejectSelectImages() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_rejected');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['approve' => 'required|numeric'])):
            $this->model->whereIn('id', $request::input('images'))->update(['approve' => $request::input('approve')]);
            if($request::has('redirect')):
                return \ResponseController::success(200)->redirect(route($request::get('redirect')))->json();
            else:
                return \ResponseController::success(200)->redirect(route('solutions.social_image_grabber.request'))->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function request() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_request');
        $images = $this->model->whereNull('approve')->orderBy('created_at', 'DESC');
        if(\Request::has('social')):
            $images = $images->where('social_title', \Request::input('social'));
        endif;
        $total_images = $images->count();
        $images = $images->paginate(30);
        return view('solutions_social_image_grabber_views::request', compact('images', 'total_images'));
    }

    public function setWinner() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_approve');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['image_id' => 'required|numeric', 'winner' => 'required|numeric'])):
            $this->model->replace($request::input('image_id'), $request);
            if($request::has('redirect')):
                return \ResponseController::success(200)->redirect(route($request::get('redirect')))->json();
            elseif($request::has('redirect_url')):
                return \ResponseController::success(200)->redirect($request::get('redirect_url'))->json();
            else:
                return \ResponseController::success(200)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function getSocialImages() {

        \PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_request');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['search' => 'required'])):
            $countVKImages = $countInstagramImages = $countTwitterImages = 0;
            foreach(explode("\n", $request::input('search')) as $search):
                if(mb_substr($search, 0, 1) == '#'):
                    if(settings(['solutions_social_image_grabber', 'social_image_grabber', 'vkontakte'])):
                        $result = \SocialImageGrabber::vkontakte(mb_substr($search, 1));
                        $countVKImages = $this->saveNewImages($result, 'vkontakte');
                    endif;
                    if(settings(['solutions_social_image_grabber', 'social_image_grabber', 'twitter'])):
                        $result = \SocialImageGrabber::twitter(mb_substr($search, 1));
                        $countTwitterImages = $this->saveNewImages($result, 'twitter');
                    endif;
                endif;
            endforeach;
            $responseText = '<nobr>VKontakte: '.$countVKImages.' '.\Lang::choice('solutions_social_image_grabber_lang::sig.images', $countVKImages).'</nobr><br>';
            $responseText .= '<nobr>Twitter: '.$countTwitterImages.' '.\Lang::choice('solutions_social_image_grabber_lang::sig.images', $countTwitterImages).'</nobr>';
            return \ResponseController::set('status', TRUE)
                ->set('responseText', $responseText)
                ->redirect(route('solutions.social_image_grabber.request'))
                ->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    private function saveNewImages(Collection $apiResult, $social_title) {

        $countNewImages = 0;
        if($apiResult->count()):
            $images = $this->model->whereSocialTitle($social_title)->lists('image_id', 'image_id');
            foreach($apiResult as $user_information):
                foreach($user_information['image_links'] as $image_id => $image_path):
                    if(!isset($images[$image_id])):
                        $model = new SocialImageGrabber();
                        $model->approve = NULL;
                        $model->image_id = $image_id;
                        $model->social_title = $social_title;
                        $model->image_link_original = $image_path;
                        $model->image_link = $image_path;
                        $model->image_likes = isset($user_information['likes']) ? $user_information['likes'] : 0;
                        $model->user_id = isset($user_information['user']['id']) ? $user_information['user']['id'] : $user_information['user_id'];
                        $model->user_name = isset($user_information['user']['name']) ? $user_information['user']['name'] : NULL;
                        $model->user_image_original = isset($user_information['user']['image']) ? $user_information['user']['image'] : NULL;
                        $model->user_image = isset($user_information['user']['image']) ? $user_information['user']['image'] : NULL;
                        $model->created_at = Carbon::now();
                        $model->updated_at = Carbon::now();
                        $model->save();
                        $countNewImages++;
                    endif;
                endforeach;
            endforeach;
        endif;
        return $countNewImages;
    }
}