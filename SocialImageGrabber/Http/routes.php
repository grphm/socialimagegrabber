<?php
\Route::group(['prefix' => 'admin/social-image-grabber', 'middleware' => 'secure'], function() {

    $this->get('approved', ['as' => 'solutions.social_image_grabber.approved', 'uses' => 'ModuleController@approved']);
    $this->post('approve', ['as' => 'solutions.social_image_grabber.approve', 'uses' => 'ModuleController@approveImage']);
    $this->post('approve-selected', ['as' => 'solutions.social_image_grabber.approve_selected', 'uses' => 'ModuleController@approveSelectImages']);
    $this->get('rejected', ['as' => 'solutions.social_image_grabber.rejected', 'uses' => 'ModuleController@rejected']);
    $this->post('reject', ['as' => 'solutions.social_image_grabber.reject', 'uses' => 'ModuleController@rejectImage']);
    $this->post('reject-selected', ['as' => 'solutions.social_image_grabber.reject_selected', 'uses' => 'ModuleController@rejectSelectImages']);
    $this->get('request', ['as' => 'solutions.social_image_grabber.request', 'uses' => 'ModuleController@request']);
    $this->post('request', ['as' => 'solutions.social_image_grabber.images', 'uses' => 'ModuleController@getSocialImages']);
    $this->post('winner', ['as' => 'solutions.social_image_grabber.winner', 'uses' => 'ModuleController@setWinner']);
});