<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSocialImageGrabberTables extends Migration {

    public function up() {

        Schema::create('social_image_grabber', function(Blueprint $table) {

            $table->increments('id');
            $table->string('social_title', 20)->nullable()->index();
            $table->boolean('approve')->nullable()->index();
            $table->bigInteger('image_id', FALSE, TRUE)->nullable()->index();
            $table->string('image_link_original', 255)->nullable();
            $table->string('image_link', 255)->nullable();
            $table->integer('image_likes', FALSE, TRUE)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable();
            $table->string('user_name', 100)->nullable();
            $table->string('user_image_original', 255)->nullable();
            $table->string('user_image', 255)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('social_image_grabber');
    }
}

