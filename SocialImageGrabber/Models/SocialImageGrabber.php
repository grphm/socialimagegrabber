<?php
namespace STALKER_CMS\Solutions\SocialImageGrabber\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class SocialImageGrabber extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'social_image_grabber';
    protected $fillable = [];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function insert($request) {

        $this->approve = NULL;
        $this->social_title = $request['social_title'];
        $this->image_id = $request['image_id'];
        $this->image_link_original = $request['image_link_original'];
        $this->image_link = $request['image_link'];
        $this->image_likes = $request['image_likes'];
        $this->user_id = $request['user_id'];
        $this->user_name = $request['user_name'];
        $this->user_image_original = $request['user_image_original'];
        $this->user_image = $request['user_image'];
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        if($request::has('approve')):
            $model->approve = $request::input('approve');
        endif;
        if($request::has('winner')):
            $model->winner = $request::input('winner');
        endif;
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['image_id' => 'required'];
    }

    public static function getUpdateRules() {

        return ['image_id' => 'required'];
    }
}