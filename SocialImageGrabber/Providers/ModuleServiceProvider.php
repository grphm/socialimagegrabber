<?php
namespace STALKER_CMS\Solutions\SocialImageGrabber\Providers;

use STALKER_CMS\Solutions\SocialImageGrabber\Http\Controllers\ImageImportController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_social_image_grabber_views');
        $this->registerLocalization('solutions_social_image_grabber_lang');
        $this->registerConfig('solutions_social_image_grabber::config', 'Config/socialimagegrabber.php');
        $this->registerSettings('solutions_social_image_grabber::settings', 'Config/settings.php');
        $this->registerActions('solutions_social_image_grabber::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_social_image_grabber::menu', 'Config/menu.php');
    }

    public function register() {

        \App::bind('ImageImportController', function() {

            return new ImageImportController();
        });
    }
}
