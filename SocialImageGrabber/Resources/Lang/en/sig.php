<?php
return [
    'search' => 'Enter to search for tags',
    'search_submit' => 'Search images',
    'search_help_block' => 'The new tag is entered on a new line. The # symbol is required to enter',
    'all_socials' => 'All networks',
    'select_all' => 'Select all',
    'images' => 'image|image|images',
    'total' => 'Total',
    'empty' => 'No images there. Use the search form to obtain a new image',
    'approve' => [
        'question' => 'Approve images',
        'confirmbuttontext' => 'Yes, to approve',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Approve'
    ],
    'reject' => [
        'question' => 'Reject images',
        'confirmbuttontext' => 'Yes, reject',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Reject'
    ]
];