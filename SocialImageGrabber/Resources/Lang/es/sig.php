<?php
return [
    'search' => 'Enter para buscar etiquetas',
    'search_submit' => 'Busca imágenes',
    'search_help_block' => 'La nueva etiqueta se introduce en una nueva línea. Se requiere que el símbolo # para entrar',
    'all_socials' => 'Todas las redes',
    'select_all' => 'Seleccionar todo',
    'images' => 'imagen|imagen|imágenes',
    'total' => 'Total',
    'empty' => 'Imágenes allí. Utilice el formulario de búsqueda para obtener una nueva imagen',
    'approve' => [
        'question' => 'Aprobar la imagen',
        'confirmbuttontext' => 'Sí, para aprobar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Approve'
    ],
    'reject' => [
        'question' => 'Rechazar la imagen',
        'confirmbuttontext' => 'Sí, rechazo',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Rechazar'
    ]
];