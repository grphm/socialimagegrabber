@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="" class="disabled">
                <i class="{{ config('solutions_social_image_grabber::menu.icon') }}"></i> {!! array_translate(config('solutions_social_image_grabber::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_social_image_grabber::menu.menu_child.social_image_request.icon') }}"></i> {!! array_translate(config('solutions_social_image_grabber::menu.menu_child.social_image_request.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_social_image_grabber::menu.menu_child.social_image_request.icon') }}"></i> {!! array_translate(config('solutions_social_image_grabber::menu.menu_child.social_image_request.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['route' => 'solutions.social_image_grabber.images', 'class' => 'form-validate' , 'id' => 'search-solutions-sig-form']) !!}
            <div class="form-group fg-line">
                <div class="fg-line">
                    <label>@lang('solutions_social_image_grabber_lang::sig.search')</label>
                    {!! Form::textarea('search', NULL, ['class' => 'form-control auto-size m-t-10', 'data-autosize-on' => 'true', 'rows' => 1, 'autocomplete' => 'off']) !!}
                </div>
                <small class="help-description">@lang('solutions_social_image_grabber_lang::sig.search_help_block')</small>
            </div>
            <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                <i class="fa fa-search"></i>
                <span class="btn-text">@lang('solutions_social_image_grabber_lang::sig.search_submit')</span>
            </button>

            {!! Form::close() !!}
        </div>
    </div>
    <div class="card">
        <div class="lv-header-alt clearfix">
            <h2 class="lvh-label hidden-xs">
                @set($allow_socials, ['vkontakte' => 'ВКонтакте', 'twitter' => 'Twitter', 'instagram' => 'Instagram'])
                @if(isset($allow_socials[\Request::input('social')]))
                    {{ $allow_socials[\Request::input('social')] }}:
                @else
                    @lang('solutions_social_image_grabber_lang::sig.total'):
                @endif
                {{ $total_images }} @choice(Lang::get('solutions_social_image_grabber_lang::sig.images'), $total_images)
            </h2>
            <ul class="lv-actions actions">
                <li class="dropdown">
                    <a aria-haspopup="true" aria-expanded="true" data-toggle="dropdown" href="">
                        <i class="zmdi zmdi-filter-list"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('solutions.social_image_grabber.request') }}">@lang('solutions_social_image_grabber_lang::sig.all_socials')</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('solutions.social_image_grabber.request') . '?social=vkontakte' }}">ВКонтакте</a>
                        </li>
                        <li>
                            <a href="{{ route('solutions.social_image_grabber.request') . '?social=twitter' }}">Twitter</a>
                        </li>
                        <li>
                            <a href="{{ route('solutions.social_image_grabber.request') . '?social=instagram' }}">Instagram</a>
                        </li>
                    </ul>
                </li>
                @if($images->count())
                    <li class="dropdown">
                        <a aria-expanded="true" data-toggle="dropdown" href="">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0);" class="js-check-all">
                                    <i class="zmdi zmdi-check-all zmdi-hc-fw"></i> @lang('solutions_social_image_grabber_lang::sig.select_all')
                                </a>
                            </li>
                            @if(\PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_approve', FALSE))
                                <li class="divider"></li>
                                <li class="pull-right">
                                    {!! Form::open(['route' => ['solutions.social_image_grabber.approve_selected'], 'class' => 'selected-approve-form']) !!}
                                    {!! Form::hidden('approve', 1) !!}
                                    <button type="submit"
                                            class="form-confirm-warning btn-link  c-green p-r-15"
                                            autocomplete="off"
                                            data-question="@lang('solutions_social_image_grabber_lang::sig.approve.question')?"
                                            data-confirmbuttontext="@lang('solutions_social_image_grabber_lang::sig.approve.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('solutions_social_image_grabber_lang::sig.approve.cancelbuttontext')">
                                        @lang('solutions_social_image_grabber_lang::sig.approve.submit')
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                            @endif
                            @if(\PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_rejected', FALSE))
                                <li class="divider pull-right w-100"></li>
                                <li class="pull-right">
                                    {!! Form::open(['route' => ['solutions.social_image_grabber.reject_selected'], 'class' => 'selected-reject-form']) !!}
                                    {!! Form::hidden('approve', 0) !!}
                                    <button type="submit"
                                            class="form-confirm-warning btn-link c-red p-r-15"
                                            autocomplete="off"
                                            data-question="@lang('solutions_social_image_grabber_lang::sig.reject.question')?"
                                            data-confirmbuttontext="@lang('solutions_social_image_grabber_lang::sig.reject.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('solutions_social_image_grabber_lang::sig.reject.cancelbuttontext')">
                                        @lang('solutions_social_image_grabber_lang::sig.reject.submit')
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <div class="card-body card-padding">
            @if($images->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($images as $image)
                            <div class="js-item-container lv-item media">
                                <div class="checkbox pull-left">
                                    <label>
                                        {!! Form::checkbox('images[]', $image->id) !!}
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                                @if($image->image_link)
                                    <div class="pull-left lightbox clearfix">
                                        <div data-src="{{ $image->image_link }}">
                                            <div class="lightbox-item p-item">
                                                <img alt="{{ $image->user_name }}" src="{{ $image->image_link }}"
                                                     class="lv-img h-50 brd-0">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="media-body">
                                    <div class="lv-title">
                                        @set($social_title, config('solutions_social_image_grabber::config.users_profiles.'.$image->social_title))
                                        @if($image->social_title == 'vkontakte')
                                            <a href="{{ $social_title. $image->user_id }}"
                                               target="_blank">{{ $image->user_name ? $image->user_name : $image->user_id  }}</a>
                                        @elseif($image->social_title == 'instagram')
                                            <a href="{{ $social_title. $image->user_name }}"
                                               target="_blank">{{ $image->user_name ? $image->user_name : $image->user_id  }}</a>
                                        @elseif($image->social_title == 'twitter')
                                            <a href="{{ $social_title. $image->user_name }}"
                                               target="_blank">{{ $image->user_name ? $image->user_name : $image->user_id  }}</a>
                                        @endif
                                    </div>
                                    <small class="lv-small">
                                        {{ $allow_socials[$image->social_title] }}, ID: {{ $image->image_id }},
                                        Likes: {{ $image->image_likes }}
                                    </small>
                                    <ul class="lv-attrs">
                                        @if(\PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_approve', FALSE))
                                            <li>
                                                {!! Form::open(['route' => 'solutions.social_image_grabber.approve']) !!}
                                                {!! Form::hidden('image_id', $image->id) !!}
                                                {!! Form::hidden('approve', 1) !!}
                                                <button type="submit" class="btn-link c-green" autocomplete="off">
                                                    @lang('solutions_social_image_grabber_lang::sig.approve.submit')
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        @endif
                                        @if(\PermissionsController::allowPermission('solutions_social_image_grabber', 'social_image_rejected', FALSE))
                                            <li>
                                                {!! Form::open(['route' => 'solutions.social_image_grabber.reject']) !!}
                                                {!! Form::hidden('image_id', $image->id) !!}
                                                {!! Form::hidden('approve', 0) !!}
                                                <button type="submit" class="btn-link c-red" autocomplete="off">
                                                    @lang('solutions_social_image_grabber_lang::sig.reject.submit')
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {!! $images->appends(['social' => \Request::input('social')])->render() !!}
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('solutions_social_image_grabber_lang::sig.empty')</h2>
            @endif
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        $(".selected-approve-form button[type='submit']").click(function () {
            $(".selected-approve-form .js-dynamic-append").remove();
            $(".listview input[type='checkbox']:checked").each(function (index, element) {
                $(".selected-approve-form").append('<input type="hidden" class="js-dynamic-append" name="images[]" value="' + $(element).val() + '">');
            })
        });
        $(".selected-reject-form button[type='submit']").click(function () {
            $(".selected-reject-form .js-dynamic-append").remove();
            $(".listview input[type='checkbox']:checked").each(function (index, element) {
                $(".selected-reject-form").append('<input type="hidden" class="js-dynamic-append" name="images[]" value="' + $(element).val() + '">');
            })
        });
    </script>
@stop
@section('scripts_after')
    <script>
        $(".js-check-all").click(function () {
            $(".listview input[type='checkbox']").prop('checked', true);
            $(this).parents('li.dropdown').removeClass('open');
        });
        BASIC.currentForm = $("#search-solutions-sig-form");
        BASIC.validateOptions.rules = {search: {required: true}};
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);
        $("#search-solutions-sig-form button[type='submit']").click(function (event) {
            BASIC.currentForm = $("#search-solutions-sig-form");
        });
        var submitOptions = BASIC.submitOptions;
        submitOptions.success = function (response, status, xhr, jqForm) {
            $(jqForm).find('button[type="submit"]').elementDisabled(false);
            if (response.status) {
                BASIC.notify(null, response.responseText, 'bottom', 'center', null, 'success', 'animated flipInX', 'animated flipOutX');
                setTimeout(function () {
                    $(jqForm).parents('.js-item-container').remove();
                    if ($(".js-item-container").length == 0) {
                        window.location.reload();
                    }
                }, 1000);
            } else {
                BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'warning', 'animated flipInX', 'animated flipOutX');
            }
            if (response.redirectURL !== false) {
                setTimeout(function () {
                    BASIC.RedirectTO(response.redirectURL);
                }, 2000);
            }
        };
        $(".js-item-container form button[type='submit']").click(function (event) {
            event.preventDefault();
            BASIC.currentForm = $(this).parents('form');
            $(this).parents('form').ajaxSubmit(submitOptions);
        });
    </script>
@stop