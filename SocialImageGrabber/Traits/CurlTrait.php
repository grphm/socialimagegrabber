<?php
namespace STALKER_CMS\Solutions\SocialImageGrabber\Traits;

trait CurlTrait {

    private function getHeaders() {

        return [
            'Content-Type: application/json',
            'Content-Length: '.$this->ContentLength
        ];
    }

    public function getCurl($url, $headers = TRUE) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        if($headers):
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
        endif;
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data['curl_result'] = curl_exec($ch);
        $data['curl_info'] = curl_getinfo($ch);
        $data['curl_headers'] = $this->getHeaders();
        curl_close($ch);
        return $data;
    }

    public function postCurl($url, $post_data = NULL, $headers = TRUE) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        if($headers):
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
        endif;
        if(!is_null($post_data)):
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        endif;
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data['curl_result'] = curl_exec($ch);
        $data['curl_info'] = curl_getinfo($ch);
        if(!is_null($post_data)):
            $data['curl_data'] = $post_data;
        endif;
        if($headers):
            $data['curl_headers'] = $this->getHeaders();
        endif;
        curl_close($ch);
        return $data;
    }

    public function putCurl($url, $post_data = NULL) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data['curl_result'] = curl_exec($ch);
        $data['curl_info'] = curl_getinfo($ch);
        $data['curl_headers'] = $this->getHeaders();
        curl_close($ch);
        return $data;
    }
}